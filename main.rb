require_relative './classes/funcionario'
require_relative './classes/projeto'
require_relative './classes/departamento'

funcionario1 = Funcionario.new("Tiago", "M", "121232", "Rua Barao do Itabagipe", "01/05/1980", 1500, "Desenvolvedor")
funcionario2 = Funcionario.new("Luiz", "M", "34765", "Rua das Flores", "03/07/1996", 1750, "Gerente")
funcionario3 = Funcionario.new("Renata", "F", "87447", "Rua das Rosas", "03/12/1997", 1600, "Desenvolvedor")
funcionario4 = Funcionario.new("Melissa", "M","67267", "Rua das Samambaias", "07/05/1996", 1200, "Desenvolvedor")
funcionario5 = Funcionario.new("Amanda", "M","67267", "Rua das Samambaias", "07/11/2001", 2500, "Gerente")

departamento1 = Departamento.new(1, funcionario2, "Back-End", "1 andar")
departamento2 = Departamento.new(1, funcionario2, "Front-End", "1 andar")
departamento3 = Departamento.new(1, funcionario5, "Gestão de Pessoas", "2 andar")

projeto1 = Projeto.new(1, "T&D Ltda", "Criação de WebDev", departamento1)
projeto2 = Projeto.new(2, "Dove Estiamo", "Aplicação Desktop", departamento2)

departamento1.adicionarFuncionario(funcionario1)
departamento1.adicionarFuncionario(funcionario2)
departamento1.adicionarFuncionario(funcionario4)
departamento2.adicionarFuncionario(funcionario3)
departamento2.adicionarFuncionario(funcionario4)
departamento3.adicionarFuncionario(funcionario5)
departamento3.adicionarFuncionario(funcionario4)

departamento1.listarFuncionarios
departamento2.listarFuncionarios
departamento3.listarFuncionarios

departamento1.retiraFuncionario(funcionario1)
departamento1.retiraFuncionario(funcionario3)
departamento1.listarFuncionarios


projeto1.adicionarDev(funcionario1)
projeto1.adicionarDev(funcionario2)
projeto2.adicionarDev(funcionario3)
projeto2.adicionarDev(funcionario4)
projeto1.adicionarDev(funcionario5)
projeto2.adicionarDev(funcionario4)

projeto1.listarDevs
projeto2.listarDevs


departamento1.adicionarProjeto(projeto1)
departamento1.adicionarProjeto(projeto2)
departamento1.adicionarProjeto(projeto1)







