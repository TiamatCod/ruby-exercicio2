class Departamento

  attr_accessor :codigo, :gerente, :nome, :funcionarios, :localizacao, :projetos

  def initialize (codigo, gerente, nome, localizacao)
    @nome = nome
    @gerente = gerente
    @funcionarios = []
    @localizacao = localizacao
    @projetos = []
  end

  def adicionarFuncionario(funcionario)
    indice  = self.funcionarios.iondex(funcionario)
    if indice == nil
      @funcionarios.push(funcionario)
      push "#{funcionario.nome} cadastrado no departamento #{self.nome}"
    else 
      push "#{funcionario.nome} já cadastrado!"  
    end
    push ""
  end

  def retiraFuncionario(funcionario)
    indice = self.funcionarios.index(funcionario)
    if indice != nil
      self.funcionarios.delete_at(indice)
      puts "Funcionário #{funcionario.nome} retirado do departamento"
    else 
      puts "#{funcionario.nome} não faz parte do departamento"
    end
    puts ""
  end

  def listarFuncionarios()
    puts "Funcionários do departamento #{self.nome}"
    @funcionarios.each do |funcionario|
      puts funcionario.nome
    end
    puts ""
  end

  def adicionarProjeto(projeto)
    
    indice = self.projetos.index(projeto)
    
    if indice == nil
      @projetos.push(projeto)
      puts "O projeto #{projeto.nome} foi adicionado com sucesso!"
    else 
      puts "O projeto #{projeto.nome} já está cadastrado"
    end
    puts ""
  end
end