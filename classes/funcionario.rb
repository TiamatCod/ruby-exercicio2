
class Funcionario
  
  attr_accessor :nome, :sexo, :nss, :endereco, :dataDeNascimento, :salario, :cargo

  def initialize (nome, sexo, nss, endereco, dataDeNascimento, salario, cargo)
    @nome = nome
    @sexo = sexo
    @nss = nss
    @endereco = endereco
    @dataDeNascimento = dataDeNascimento
    @salario = salario
    @cargo = cargo
  end
end