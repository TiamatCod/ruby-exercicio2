class Projeto
  
  attr_accessor :codigo, :cliente, :nome, :departamento, :desenvolvedores

  def initialize(codigo, cliente, nome, departamento)
    @codigo = codigo
    @cliente = cliente
    @nome = nome
    @departamento = departamento
    @desenvolvedores = []
  end
  
  def adicionarDev(funcionario)
    if funcionario.cargo == "Desenvolvedor"
      @desenvolvedores.push(funcionario)
    else 
      puts "O funcionário #{funcionario.nome} não é desenvolvedor!"
    end
    puts ""
  end

  def listarDevs
    puts "Desenvolvedores do projeto #{self.nome}"
    self.desenvolvedores.each do |desenvolvedor|
      puts desenvolvedor.nome
    end
    puts ""
  end

end