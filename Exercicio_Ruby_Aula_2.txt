Baseado no exercicio exemplo passado na aula 2 de Modelagem de Dados. Desenvolva os scripts das classes para um sistema de gerenciamento do quadro de funcion�rios de uma empresa.

Os m�todos e atributos est�o dispostos no diagram do anexo 1.

Resumo dos M�todos:
  Classe Departamento:
    - adicionarFuncionario(funcionario)
      M�todo de inst�ncia que recebe um objeto do tipo funcion�rio e adiciona ao atributo array funcion�rio o objeto funcion�rio passado.
    - retirarFuncionario(funcionario)
      M�todo de inst�ncia que recebe um objeto do tipo funcion�rio e retira do atributo array funcion�rio o objeto funcion�rio passado.
    - listarFuncionarios()
      M�todo de inst�ncia que imprime no console os objetos presentes na array do atributo funcion�rios.
    - adicionarProjeto(projeto)
      M�todo de inst�ncia que recebe um objeto do tipo projeto e adiciona ao atributo array projeto o objeto projeto passado.

  Classe Projeto
    - listarDevs(desenvolvedor)
      M�todo de inst�ncia que imprime no console os objetos presentes na array do atributo desenvolvedores.
     - adicionarDev(desenvolvedor)
      M�todo de inst�ncia que recebe um objeto do tipo desenvolvedor e adiciona ao atributo array desenvolvedor o objeto desenvolvedor passado.
